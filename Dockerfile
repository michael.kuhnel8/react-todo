FROM node:20-alpine

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install -g eslint
RUN npm install -g jest
RUN npm install -g globals
RUN npm install

COPY . .
