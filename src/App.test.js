import { render, screen, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import App from './App';
import React from 'react';

test('App integrates components and handles interactions', () => {
    render(<App/>);

    const input = screen.getByPlaceholderText(/Co chcete přidat.../i);
    fireEvent.change(input, {target: {value: 'Learn React'}});
    fireEvent.keyDown(input, {key: 'Enter', keyCode: 13});
    expect(screen.getByText('Learn React')).toBeInTheDocument();

    const learnReactRow = screen.getByText('Learn React').closest('li');
    const checkbox = learnReactRow.querySelector('input[type="checkbox"]');
    const isAlreadyChecked = checkbox.checked;
    fireEvent.click(checkbox);
    expect(checkbox.checked).toBe(!isAlreadyChecked);

    const editButton = learnReactRow.querySelector('svg.fa-pen-to-square');
    fireEvent.click(editButton);
    const inputInRow = learnReactRow.querySelector('input[value="Learn React"]');
    fireEvent.change(inputInRow, {target: {value: 'Learn React Edited'}});
    fireEvent.keyDown(inputInRow, {key: 'Enter', keyCode: 13});
    expect(screen.getByText('Learn React Edited')).toBeInTheDocument();

    const deleteButton = learnReactRow.querySelector('svg.fa-trash');
    fireEvent.click(deleteButton);
    expect(screen.queryByText('Learn React Edited')).toBeNull();

});
