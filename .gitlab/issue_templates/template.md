-   [ ] Verify Gitlab pages.
-   [ ] Create a new brach and modify code with a new method, e.g.:
    ```python
    def deactivate(self):
        self.profile['active'] = False
    ```
-   [ ] Verify the new MR is having code coverage percentage report.
-   [ ] Create README.md and add project badges to it:
    -   [ ] CI status
    -   [ ] Code coverage status
